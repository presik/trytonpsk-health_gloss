# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.modules.company import CompanyReport

__all__ = ['Gloss', 'GlossReport']

STATES = {'readonly': (Eval('state') != 'draft'),}
STATES_PROCESSED = {'readonly': (Eval('state') != 'draft'),}


class Gloss(Workflow, ModelSQL, ModelView):
    'Gloss'
    __name__ = 'gnuhealth.gloss'
    _rec_name = 'file_number'
    number = fields.Char('Number', select=True, states=STATES)
    party = fields.Many2One('party.party', 'Party', required=True,
        select=True, states=STATES)
    category = fields.Many2One('gnuhealth.gloss_category', 'Category',
        select=True, states=STATES)
    received_date = fields.Date('Received Date', required=True, states=STATES)
    response_date = fields.Date('Response Date', states=STATES)
    kind_party = fields.Selection([
            ('regimen_subsidiado', 'Regimen Subsidiado'),
            ('regimen_contributivo', 'Regimen Contributivo'),
            ('soat', 'SOAT'),
            ], 'Kind Party')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('process', 'Process'),
            ('canceled', 'Canceled'),
            ('accepted', 'Accepted'),
            ('rejected', 'Rejected'),
            ('reprocess', 'Reprocess'),
            ], 'State', readonly=True)
    reference = fields.Char('Reference', select=True, required=True,
        states=STATES)
    out_invoice = fields.Many2One('account.invoice', 'Out Invoice',
        select=True, domain=[
            ('type', '=', 'out_invoice')
        ], states=STATES)
    out_credit_note = fields.Many2One('account.invoice', 'Out Credit Note',
        select=True, domain=[
            ('type', '=', 'out_credit_note')
        ], states=STATES)
    employee = fields.Many2One('company.employee', 'Employee',
        select=True, states=STATES)
    description = fields.Text('Description', states=STATES)
    response = fields.Text('Response', states=STATES)
    is_reconciled = fields.Boolean('Is Reconciled', states=STATES)

    @classmethod
    def __setup__(cls):
        super(Gloss, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'process'),
                ('process', 'draft'),
                ('draft', 'cancel'),
                ('cancel', 'draft'),
                ('process', 'accepted'),
                ('process', 'rejected'),
                ('rejected', 'reprocess'),
                ('reprocess', 'accepted'),
                ('reprocess', 'rejected'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': (Eval('state') != 'draft'),
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(['canceled', 'process']),
                    },
                'process': {
                    'invisible': (Eval('state') != 'draft'),
                    },
                'accept': {
                    'invisible': (Eval('state') != 'process'),
                    },
                'reject': {
                    'invisible': (Eval('state') != 'process'),
                    },
                'reprocess': {
                    'invisible': (Eval('state') != 'rejected'),
                    },
                })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_received_date():
        Date = Pool().get('ir.date')
        return Date.today()


    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, invoices):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, invoices):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('process')
    def process(cls, invoices):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('accepted')
    def accept(cls, invoices):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('rejected')
    def reject(cls, invoices):
        pass
    
    @classmethod
    @ModelView.button
    @Workflow.transition('reprocess')
    def reprocess(cls, invoices):
        pass


class GlossReport(CompanyReport):
    __name__ = 'gnuhealth.gloss'
