# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta

__all__ = ['GlossCategory']


class GlossCategory(ModelSQL, ModelView):
    'Gloss Category'
    __metaclass__ = PoolMeta
    __name__ = 'gnuhealth.gloss_category'
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')

    @classmethod
    def __setup__(cls):
        super(GlossCategory, cls).__setup__()
